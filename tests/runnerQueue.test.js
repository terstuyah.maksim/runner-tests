const {asyncRunnerQueue} = require('../index');

const sum = (a, b) => a+b;
const pow = (a, b) => a**b;
const mul = (a, b) => a*b;
const diff = (a, b) => a-b;
const applyAsync = (fn, t = 0) => (...args) => setTimeout(args.pop(), t, fn(...args));
let asyncFnc = [
  applyAsync(diff),
  applyAsync(sum, 100),
  applyAsync(mul, 1000),
  applyAsync(pow, 2000),
];

describe('tests for runner', () => {
  test('result of each function of asyncFn array should have the same index in the result array ', (done) => {

    global.console.log = (result) => {
      expect(result[0]).toBe(0);
      expect(result[1]).toBe(3);
      expect(result[2]).toBe(9);
      expect(result[3]).toBe(19683);
      done();
    }

    asyncRunnerQueue(3, 3)(asyncFnc);
  });

  test('should call each function of asyncFn array with second argument as a previous function result, starting out th second function', (done) => {

    const mockFnSum = jest.fn((a,b) => a+b);
    const mockFnMul = jest.fn((a,b) => a*b);
    const mockFnPow = jest.fn((a,b) => a**b);

    global.console.log = (result) => {
      expect(mockFnSum).toBeCalledWith(3, 0);
      expect(mockFnMul).toBeCalledWith(3, 3);
      expect(mockFnPow).toBeCalledWith(3, 9);
      done();
    }

    asyncFnc = [
      applyAsync(diff, 2000),
      applyAsync(mockFnSum, 1000),
      applyAsync(mockFnMul, 100),
      applyAsync(mockFnPow),
    ];

    asyncRunnerQueue(3, 3)(asyncFnc);
  });


  test('should be end the same time sum times each function of asyncFn array has', (done) => {
    let date = Date.now();
    global.console.log = (result) => {
      const dateEnd = Date.now() - date;
      expect(dateEnd < 3400).toBeTruthy();
      done();
    }
    asyncRunnerQueue(3, 3)(asyncFnc);
  });

  test('should have  the same length', (done) => {
    global.console.log = (result) => {
      expect(result.length).toBe(4);
      done();
    }
    asyncRunnerQueue(3, 3)(asyncFnc);
  });
});
