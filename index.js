function asyncRunner(...args) {
  const result = [];
  return (fns) => {
    fns.forEach(fn => fn(...args, cb => {
      result.push(cb);
      if(result.length === fns.length){
        console.log(result)
      }
    }))
  };
}


function asyncRunnerQueue(a,...args) {
  const result = []
  let i = 0
  return (asyncFns) => { 
  const funcRecursion = i =>(num) => {
    result.splice(i, 0, num)
    if(result.length === asyncFns.length){            
      console.log(result)
      return;
    }
    (i+=1, asyncFns[i](a, num, (res) => funcRecursion(i)(res)))
  };
  asyncFns[i]( a, ...args, (res) => (
    funcRecursion(i)(res) ));
  };
}


module.exports = {asyncRunner, asyncRunnerQueue};