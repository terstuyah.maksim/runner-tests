const runner = (...args) => fns => fns.map(fn => fn(...args));
const sum = (a, b = 0) => a+b;
const pow = (a, b = 1) => a**b;
const mul = (a, b = 1) => a*b;
const dif = (a, b = 0) => a-b;
const fns = [dif, sum, mul, pow];
const result = runner(3, 3)(fns);
console.log(result);

const applyAsync = (fn,t) => (...args) => setTimeout(args.pop(), t, fn(...args));
const asyncFns = [
  applyAsync(dif),
  applyAsync(sum, 100),
  applyAsync(mul, 1000),
  applyAsync(pow, 2000),
];

function asyncRunner(...args) {
  const result = [];
  return (fns) => {
    fns.forEach(fn => fn(...args, cb => {
      result.push(cb);
      if(result.length === fns.length){
        console.log(result)
      }
    }))
  };
}
    
asyncRunner(3,3)(asyncFns)



function asyncRunnerQueue(a,b) {
  const result = []
  let i = 0
  return (asyncFns) => {      
  // console.log(asyncFns);
  const funcRunner = i =>(num) => {

      // result[i] = num
      result.splice(i, 0, num)
      // console.log('counter', i);
      // console.log('num', num);
      // console.log("result.length", result.length);
      // console.log('asyncFns.lenght', asyncFns.length);

      if(result.length === asyncFns.length){            
          console.log(result)
          return;
      }
      i+=1
      asyncFns[i](a, num, (res) => funcRunner(i)(res))   
  };
  
  asyncFns[i]( a,b, (res) => ( 
    // console.log(i, a, ...args, (res)), 
    funcRunner(i)(res) ));
  };
}

asyncRunnerQueue(3,3)(asyncFns)